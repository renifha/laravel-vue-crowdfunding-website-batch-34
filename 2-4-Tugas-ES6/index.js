// soal 1
const luasPersegi = (p, l) => {
    return p*l
}

const kelilingPersegi = (p, l) => {
    return 2*(p+l)
}

console.log(luasPersegi(2, 3))
console.log(kelilingPersegi(2, 3))

// soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
        }
    }
}
 
newFunction("William", "Imoh").fullName() 

// soal 3
var newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
console.log(combinedArray)

// soal 5
const planet = "earth" 
const view = "glass" 

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)

