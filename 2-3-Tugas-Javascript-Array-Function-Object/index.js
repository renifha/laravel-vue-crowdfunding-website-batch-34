// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
daftarHewan.forEach(function(item){
    console.log(item)
})


// Soal 2
function introduce(bio) {
    return "Nama saya "+bio.name+", umur saya "+bio.age+" tahun, alamat saya di "+bio.address+", dan saya punya hobby yaitu "+bio.hobby;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// Soal 3
function hitung_huruf_vokal(str) {
    var daftarHurufVokal = "aiueo";
    var hitung = 0;
    for(var i=0; i<str.length; i++) {
        if(daftarHurufVokal.indexOf(str.toLowerCase()[i])!==-1){
            hitung+=1;
        }
    }
    return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


// Soal 4
function hitung(a) {
    return 2*a-2;
};
console.log( hitung(0) ) 
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) ) 