<?php
trait Hewan {
    public $nama;
    public $darah=50;
    public $jumlahKaki;
    public $keahlian;

    
    public function atraksi() {
    echo "{$this->nama} sedang {$this->keahlian}.";
    }
}

abstract class Fight {
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan) {
        echo "{$this->nama} sedang menyerang {$hewan->nama}. <br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan) {
        echo "{$this->nama} sedang diserang {$hewan->nama}.";
        $this->darah = $this->darah - $hewan->attackPower / $this->defencePower;
    }

    protected function getInfo() {
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defence Power : {$this->defencePower}";
        echo "<br><br>";
        $this->atraksi();
    }

    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($nama, $keahlian) {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = $keahlian;
        $this->attackPower=10;
        $this->defencePower=5;
    }

    public function getInfoHewan() {
        echo "Jenis Hewan : Elang";
        $this->getInfo();
    }
}

class Harimau extends Fight{
    public function __construct($nama, $keahlian) {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = $keahlian;
        $this->attackPower=10;
        $this->defencePower=5;
    }

    public function getInfoHewan() {
        echo "Jenis Hewan : Harimau";
        $this->getInfo();
    }
}

class baris{
    public static function buatBaris() {
        echo "<br>";
        echo "===========";
        echo "<br>";
    }
}

// output
$elang = new Elang("harimau_1", "lari cepat");
$elang->getInfoHewan();
baris::buatBaris();
$harimau = new Harimau("elang_3", "terbang tinggi");
$harimau->getInfoHewan();
baris::buatBaris();
$elang->serang($harimau);
baris::buatBaris();
$harimau->getInfoHewan();
