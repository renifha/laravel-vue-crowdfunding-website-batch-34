<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UpdateProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'photo_profile' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $new_photo = $request->photo_profile;
        $new_photo->move('photo/', $new_photo);

        $request->user()->forceFill([
            'name'=>$request->name,
            'photo_profile' => $new_photo,
        ])->save();

        return response()->json([
            'response_code'=>'00',
            'response_message'=>'profile berhasil diupdate',
            'data'=> [
                'user' => auth()->user()
            ]
            ]);   
        
    }
}
