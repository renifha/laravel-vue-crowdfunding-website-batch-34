<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Events\RegenerateOtpCodeEvent;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //kirim ulang ke email
        $request->validate([
            'email'=>'required'
        ]);

        $user = User::where('email', $request->email)->first();

        $user->generate_otp_code();

        event(new RegenerateOtpCodeEvent($user));

        $data['user']=$user;

        return response()->json([
            'response_code'=>'00',
            'response_message'=>'otp berhasil digenerate',
            'data'=>$data
        ]);
    }
}
