<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();

        if($user->password != null && $user->email_verified_at != null && $user->role->name == 'admin') {
            return $next($request);
        }

        return response()->json([
            'message'=>'Email Anda belum terverifikasi atau mungkin Anda bukan Admin ya?'
        ]);
    }
}
