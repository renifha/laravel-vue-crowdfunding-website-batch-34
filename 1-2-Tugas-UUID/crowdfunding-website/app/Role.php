<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traints\UsesUuid;

class Role extends Model
{
    protected $fillable = ['name'];
    protected $primaryKey= 'id';

    use UsesUuid;

    public function user() {
        return $this->hasMany('App\User', 'role_id');
    }
}
