<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traints\UsesUuid;

class otp_code extends Model
{
    use UsesUuid;
    protected $fillable = [
        'otp', 'user_id', 'valid_until'
    ];

    public function user() {
        return $this->belongTo('App\User', 'user_id');
    }
}
