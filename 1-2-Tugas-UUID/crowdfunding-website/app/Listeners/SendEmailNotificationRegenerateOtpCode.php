<?php

namespace App\Listeners;

use App\Events\RegenerateOtpCodeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\RegenerateOtpCodeMail;


class SendEmailNotificationRegenerateOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpCodeEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpCodeEvent $event)
    {
        Mail::to($event->user)->send(new RegenerateOtpCodeMail($event->user));
    }
}
